import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from '../reducers/';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas/rootSaga'

// Saga Middleware
const sagaMiddleware = createSagaMiddleware()

// Create middlewares for redux
let middlewares = applyMiddleware(sagaMiddleware)

// Create redux store
const store = createStore(
  rootReducer,
  compose(middlewares),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

// run saga watchers
sagaMiddleware.run(rootSaga)
export default store
