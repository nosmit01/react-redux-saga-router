export const NOTES_WATCHER = "NOTES_WATCHER";
export const ADD_NOTE = "ADD_NOTE";

export function notesWatcher(text, afterIndex, notes) {
  return {
    type: NOTES_WATCHER,
    text,
    afterIndex,
    notes
  };
}

export function addNote(notes) {
  return {
    type: ADD_NOTE,
    notes
  };
}
