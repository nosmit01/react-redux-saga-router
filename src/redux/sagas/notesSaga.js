import { addNote } from '../actions/notes'
import { takeLatest, put } from 'redux-saga/effects'

function* notesEffectSaga(action) {
  const notes = [
    ...action.notes.slice(0, action.afterIndex + 1),
    action.text,
    ...action.notes.slice(action.afterIndex + 1)
  ]

  localStorage.setItem("notes", JSON.stringify(notes));
  yield put(addNote(notes))
}

export function* notesWatcherSaga() {
  yield takeLatest('NOTES_WATCHER', notesEffectSaga)
}