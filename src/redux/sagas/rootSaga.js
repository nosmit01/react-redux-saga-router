import { all } from 'redux-saga/effects'
import { notesWatcherSaga } from './notesSaga'

export default function* rootSaga() {
  yield all([
    notesWatcherSaga()
  ])
}