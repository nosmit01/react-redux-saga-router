import { ADD_NOTE } from "../actions/notes";

const notes = localStorage.getItem("notes")
const initState = notes ? JSON.parse(notes) : ["note 1", "note 2", "note 3", "note 4"];

export default function(state = initState, action) {
  switch (action.type) {
    case ADD_NOTE:
      return action.notes;
    default:
      return state;
  }
}
