import React from "react";
import { withRouter } from "react-router";
import { NavLink } from 'react-router-dom';

import styles from "./styles.module.css";

class Dashboard extends React.Component {
  render() {
    const imgExt = this.props.type === 'artists' ? 'png' : 'jpg'
    return (
      <div className={styles['dashboard']}>
        <div>
          <h3 className={styles['header']}>{this.props.title}</h3>
          {this.renderList(imgExt)}
        </div>
        {this.renderSpotLight(imgExt)}
      </div>
    );
  }

  renderList(imgExt) {
    return this.props.list.map(item => (
      <NavLink
        key={item._id}
        to={`/${this.props.type}/${item._id}`}
        className={styles['item-row']}
        activeClassName={styles['active-item-row']}
      >
        <div
          className={styles['item-avatar']}
          style={{
            backgroundImage: `url(/imgs/${this.props.type}/${item._id}.${imgExt})`
          }}
        />
        {this.props.type === 'artists' ? (
          <span>{`${item.firstName} ${item.lastName}`}</span>
        ) : (
          <span>{item.title}</span>
        )}
      </NavLink>
    ));
  }

  renderSpotLight(imgExt) {
    const props = this.props;
    const selectedId = props.match.params && props.match.params.id;
    if (!selectedId) {
      return null;
    }
    const itemInSpotlight = props.list.find(
      item => item._id === selectedId
    );
    const label = props.type === 'artists' ?
      `${itemInSpotlight.firstName} ${itemInSpotlight.lastName}` : itemInSpotlight.title;
    const imgUrl = `imgs/${props.type}/${itemInSpotlight._id}.${imgExt}`;
    return (
      <div className={styles['spotlight']}>
        <div
          className={styles['spotlight-img']}
          style={{
            backgroundImage: `url(${imgUrl})`
          }}
        />
        <div className={styles['spotlight-label']}>{label}</div>
      </div>
    );
  }
}

export default withRouter(Dashboard);
